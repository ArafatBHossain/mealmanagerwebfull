<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Meal Planner</title>
    <link href="<c:url value="/resources/css/topbar.css" />"
          rel="stylesheet" type="text/css"/>

    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>


    <script
            src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"
            type="text/javascript"></script>


</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">

    <div class="row topbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="
                     col-sm-4 col-sm-offset-1
                     col-xs-12">
            <a class="navbar-brand" href="project-groups.html">Meal Planner<sup>beta</sup></a>
        </div>
        <div class="
                     col-sm-5 col-sm-offset-1
                     col-xs-12">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/mealmanager/home">Home</a></li>
                <li><a href="/mealmanager/createmenu">Prepare Menu</a></li>
                <li><a href="/mealmanager/mealtypes">Meal Types</a></li>
                <li><a href="/mealmanager/dishes">Dishes</a></li>
                <li><a href="/mealmanager/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<center>
    <form action="edit" method="post">
        <div class="form-group">
            <fieldset>
                <legend><c:out value="${meal.getType().getName()}"></c:out> For
                    <c:out value="${meal.getDay().getName()}"></c:out></legend>

                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Dish</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${meal.getListOfDishes()}" var="dish">
                            <tr>
                                <td align="center">
                                    <button class="btn btn-primary" name="button" type="submit" value="${dish.getId()}">
                                        Delete
                                    </button>
                                </td>
                                <td><c:out value="${dish.getName()}"></c:out></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="form-group" style="color: #FF0000;">${errorMessage.getMessage()}</div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select name="dishesToAdd" multiple>
                            <c:forEach items="${allDishes}" var="dish">
                                <option value="${dish.getId()}"><c:out value="${dish.getName()}"></c:out></option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="add"></label>

                        <div class="col-md-4">
                            <input type="hidden" name="day" value="${meal.getDay().getId()}">
                            <input type="hidden" name="type" value="${meal.getType().getId()}">
                            <button id="add" name="button" type="submit" class="btn btn-primary" value="add">Add
                                Selected Dish
                            </button>

                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</center>
</body>
</html>