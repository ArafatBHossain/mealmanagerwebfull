<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Meal Planner</title>
    <link href="<c:url value="/resources/css/topbar.css" />"
          rel="stylesheet" type="text/css"/>

    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>


    <script
            src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"
            type="text/javascript"></script>


</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">

    <div class="row topbar">
        <div class="
                     col-sm-4 col-sm-offset-1
                     col-xs-12">
            <a class="navbar-brand" href="project-groups.html">Meal Planner<sup>beta</sup></a>
        </div>
        <div class="
                     col-sm-5 col-sm-offset-1
                     col-xs-12">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/mealmanager/home">Home</a></li>
                <li><a href="/mealmanager/createmenu">Prepare Menu</a></li>
                <li><a href="/mealmanager/mealtypes">Meal Types</a></li>
                <li><a href="/mealmanager/dishes">Dishes</a></li>
                <li><a href="/mealmanager/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<center>
    <form class="form-horizontal" action="createmenu" method="post">
        <fieldset>

            <legend>Prepare Menu</legend>

            <div class="form-group">
                <label class="col-md-4 control-label" for="idday">Day</label>

                <div class="col-md-4">
                    <select id="idday" name="day" class="form-control">
                        <c:forEach items="${weekdays}" var="day">
                            <option value="${day.getId()}"><c:out value="${day.getName()}"></c:out></option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="idtype">Meal type</label>

                <div class="col-md-4">
                    <select id="idtype" name="type" class="form-control">
                        <c:forEach items="${mealTypeList}" var="mealtype">
                            <option value="${mealtype.getId()}"><c:out value="${mealtype.getName()}"></c:out></option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="sel2">Select Dishes From The List (hold shift to select more
                    than one):</label>

                <div class="col-md-4">
                    <select onchange="changeFunc()" multiple class="form-control" name="dishes" id="sel2">
                        <c:forEach items="${dishList}" var="dish">
                            <option value="${dish.getId()}"><c:out value="${dish.getName()}"></c:out></option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="send"></label>

                <div class="col-md-4">
                    <button id="send" type="submit" name="prepare" class="btn btn-primary" disabled="true">Prepare</button>
                </div>
            </div>

        </fieldset>
    </form>
    <div class="form-group" style="color: #FF0000;">${errorMessage}</div>
</center>
</body>
<script>
    function changeFunc() {
        document.getElementById("send").disabled = false;
    }
</script>
</html>