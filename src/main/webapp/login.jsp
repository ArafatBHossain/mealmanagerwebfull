<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Meal Planner</title>
    <link href="<c:url value="/resources/css/topbar.css" />"
          rel="stylesheet" type="text/css"/>

    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>


    <script
            src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"
            type="text/javascript"></script>


</head>
<body>


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="row topbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div
                class="
                 col-sm-4 col-sm-offset-1
                 col-xs-12">
            <a class="navbar-brand">Meal Planner<sup>beta</sup></a>
        </div>

        <form class="form-inline" action="login" method="post">

            <div class="form-group">

                <input type="text" class="form-control" id="idname" name="name" placeholder="Username">

            </div>

            <div class="form-group">

                <label class="sr-only" for="inputPassword">Password</label>

                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">

            </div>
            <div class="form-group" style="color: #FF0000;">${errorMessage}</div>

            <button type="submit" class="btn btn-success">Login</button>

        </form>


    </div>
</nav>
<center>
    <form class="form-horizontal" action='signup' method="POST">
        <fieldset>
            <div id="legend">
                <legend class="">Register</legend>
            </div>
            <div class="control-group">
                <label class="control-label" for="username">Username</label>

                <div class="controls">
                    <input type="text" id="username" name="username" placeholder="" class="input-xlarge" >
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="password">Password</label>

                <div class="controls">
                    <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="password_confirm">Password (Confirm)</label>

                <div class="controls">
                    <input type="password" id="password_confirm" name="confirmPassword" placeholder=""
                           class="input-xlarge">
                </div>
            </div>

            <br>

            <div class="control-group">
                <!-- Button -->
                <div class="controls">
                    <button class="btn btn-success" type="submit">Register</button>
                </div>
            </div>
            <div class="form-group" style="color: #FF0000;">${signUpErrorMessage}</div>
        </fieldset>
    </form>
</center>

</body>
</html>