<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Meal Planner</title>
    <link href="<c:url value="/resources/css/topbar.css" />"
          rel="stylesheet" type="text/css"/>

    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>


    <script
            src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"
            type="text/javascript"></script>


</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">

    <div class="row topbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="
                     col-sm-4 col-sm-offset-1
                     col-xs-12">
            <a class="navbar-brand" href="project-groups.html">Meal Planner<sup>beta</sup></a>
        </div>
        <div class="
                     col-sm-5 col-sm-offset-1
                     col-xs-12">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/mealmanager/home">Home</a></li>
                <li><a href="/mealmanager/createmenu">Prepare Menu</a></li>
                <li><a href="/mealmanager/mealtypes">Meal Types</a></li>
                <li><a href="/mealmanager/dishes">Dishes</a></li>
                <li><a href="/mealmanager/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<center>
    <form class="form-horizontal" action="home" method="post">
        <fieldset>

            <legend>View Menu For Each Day</legend>

            <div class="form-group">
                <label class="col-md-4 control-label" for="idday">Day</label>

                <div class="col-md-4">
                    <select id="idday" name="day" class="form-control">
                        <c:forEach items="${weekdays}" var="day">
                            <option value="${day.getId()}" ${day.getId() == selectedDay ? 'selected="selected"' : ''}>${day.getName()}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="idtype">Meal type</label>

                <div class="col-md-4">
                    <select id="idtype" name="type" class="form-control">
                        <c:forEach items="${mealTypeList}" var="type">
                            <option value="${type.getId()}" ${type.getId() == selectedType ? 'selected="selected"' : ''}>${type.getName()}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="showmenuid"></label>

                <div class="col-md-4">
                    <button id="showmenuid" name="button" class="btn btn-primary" type="submit" value="showMenu">Show
                        Menu
                    </button>

                </div>
            </div>
        </fieldset>

        <c:if test="${not empty dishList}">
            <div class="form-group">
                <fieldset>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <table class="table table-striped table-bordered table-list">
                            <thead>
                            <tr>
                                <th>Dish</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${dishList}" var="dish">
                                <tr>
                                    <td><c:out value="${dish.getName()}"></c:out></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </fieldset>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button id="editButton" name="button" value="editMenu" type="submit" class="btn btn-primary">
                        Edit This Meal
                    </button>
                </div>
            </div>
        </c:if>

    </form>
    <div class="form-group" style="color: #FF0000;">${errorMessage}</div>

</center>

</body>
</html>