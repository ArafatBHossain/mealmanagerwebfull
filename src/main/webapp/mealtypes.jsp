<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Meal Planner</title>
    <link href="<c:url value="/resources/css/topbar.css" />"
          rel="stylesheet" type="text/css"/>

    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>


    <script
            src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"
            type="text/javascript"></script>


</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">

    <div class="row topbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="
                     col-sm-4 col-sm-offset-1
                     col-xs-12">
            <a class="navbar-brand" href="#">Meal Planner<sup>beta</sup></a>
        </div>
        <div class="
                     col-sm-5 col-sm-offset-1
                     col-xs-12">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/mealmanager/home">Home</a></li>
                <li><a href="/mealmanager/createmenu">Prepare Menu</a></li>
                <li><a href="/mealmanager/mealtypes">Meal Types</a></li>
                <li><a href="/mealmanager/dishes">Dishes</a></li>
                <li><a href="/mealmanager/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">

        <p></p>

        <p> </p>

        <p> </p>

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Available Meal Types</h3>
                        </div>
                        <div class="col col-xs-6 text-right">
                            <form class="form-inline" action="mealtypes" method="post">
                                <input type="text" class="form-control" id="idname" name="name" placeholder="Name">
                                <button type="submit" class="btn btn-sm btn-primary btn-create">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <fieldset>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <table class="table table-striped table-bordered table-list">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${mealTypeList}" var="mealType">
                                    <tr>
                                        <td><c:out value="${mealType.getName()}"></c:out></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4"></div>
                    </fieldset>
                </div>

            </div>

        </div>
    </div>

</div>
<div class="form-group" style="color: #FF0000;">${errorMessage}</div>


</body>

</html>