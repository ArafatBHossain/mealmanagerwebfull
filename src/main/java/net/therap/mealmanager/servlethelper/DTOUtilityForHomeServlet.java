package net.therap.mealmanager.servlethelper;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.service.WeekDayService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 12/1/16
 */
public class DTOUtilityForHomeServlet {

    private MealTypeService mealTypeService;
    private WeekDayService weekDayService;

    public WeekDay getDay(HttpServletRequest request) {
        weekDayService = new WeekDayService();
        return weekDayService.getWeekDay(Integer.valueOf(request.getParameter("day")));
    }

    public MealType getType(HttpServletRequest request) {
        mealTypeService = new MealTypeService();
        int id = Integer.parseInt(request.getParameter("type"));
        return mealTypeService.getMealType(id);
    }

    public void showMenuForTheMeal(int mealId, HttpServletRequest request,HttpServletResponse response
    , WeekDay day, MealType type) throws ServletException, IOException {

        request.setAttribute("selectedDay", day.getId());

        request.setAttribute("selectedType", type.getId());

        if(mealId != 0){
            MealService mealService = new MealService();
            Meal meal = mealService.getMealById(mealId);

            List<Dish> dishList = null;

            try {
                dishList = meal.getListOfDishes();
            }catch (NullPointerException e){

            }
            if(dishList == null){
                request.setAttribute("errorMessage", "Dishes not found");
            }else {
                request.setAttribute("dishList", dishList);
            }
        }

        weekDayService = new WeekDayService();
        request.setAttribute("weekdays",weekDayService.getListOfWeekDays());
        try {
            mealTypeService = new MealTypeService();
            request.setAttribute("mealTypeList", mealTypeService.getAllMealType());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    public void goToEditPage(int mealId, HttpServletRequest request,
                                           HttpServletResponse response) throws ServletException, IOException {

        request.getSession().setAttribute("mealId",mealId);

        response.sendRedirect("/mealmanager/edit");
    }
}
