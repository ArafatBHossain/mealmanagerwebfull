package net.therap.mealmanager.servlethelper;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.validation.Error;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 12/1/16
 */
public class CreateMenuServletHelper {

    public void saveMealAndRedirectToEditPage(WeekDay day, MealType type,
                                               HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        List<Dish> dishList = new ArrayList<>();

        String[] dishes = request.getParameterValues("dishes");

        DishService dishService = new DishService();
        for(String d : dishes){
            Dish dish = dishService.getDish(Integer.valueOf(d));
            dishList.add(dish);
        }

        Meal meal = setMealObject(day, type, dishList);

        saveMeal(meal);

        request.getSession().setAttribute("mealId", meal.getId());

        response.sendRedirect("/mealmanager/edit");
    }

    public Meal setMealObject(WeekDay day, MealType type, List<Dish> dishList) {
        Meal meal = new Meal();
        meal.setDay(day);
        meal.setType(type);
        meal.setListOfDishes(dishList);

        return meal;
    }

    public void saveMeal(Meal meal) {
        MealService mealService = new MealService();
        mealService.saveMeal(meal);
    }

    public void reloadPageWithErrorMessage(HttpServletRequest request, HttpServletResponse response,
                                           List<WeekDay> weekDays, List<MealType> mealTypeList)
            throws ServletException, IOException {

        Error error = new Error("Meal Exists:", "Meal Already Exists, " +
                "Please Consider Editing the Meal");

        request.setAttribute("errorMessage",error.getType()+" "+error.getMessage());
        request.setAttribute("weekdays",weekDays);
        request.setAttribute("mealTypeList",mealTypeList);

        DishService dishService = new DishService();
        request.setAttribute("dishList", dishService.getAllDishes());

        request.getRequestDispatcher("/createmenu.jsp").forward(request, response);
    }
}
