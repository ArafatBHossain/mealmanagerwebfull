package net.therap.mealmanager.servlethelper;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.validation.Error;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author arafat
 * @since 12/1/16
 */
public class DTOUtilityForEditServlet extends DTOUtilityForHomeServlet {

    private DishService dishService;
    private MealService mealService;

    public void addDishToMenu(HttpServletRequest request, HttpServletResponse response,
                              Meal meal, List<Dish> existingDishListForTheMeal)
            throws ServletException, IOException {
        String[] dishesToAdd = request.getParameterValues("dishesToAdd");

        if(containsDuplicateitem(dishesToAdd, existingDishListForTheMeal)){
            Error error = new Error("Duplicate Entry", "Similar dishes already available for the meal");

            request.setAttribute("errorMessage", error);

        }else{
            dishService = new DishService();

            for (String dish: dishesToAdd){
                Dish dishToAdd = dishService.getDish(Integer.valueOf(dish));
                existingDishListForTheMeal.add(dishToAdd);
            }
            mealService = new MealService();
            mealService.updateMeal(meal);

        }
        request.setAttribute("meal", meal);

        request.getRequestDispatcher("/edit.jsp").forward(request, response);
    }

    private boolean containsDuplicateitem(String[] dishesToAdd, List<Dish> existingDishListForTheMeal) {
        boolean doesContain = false;
        dishService = new DishService();

        for(String id : dishesToAdd){
            Dish dishToAdd = dishService.getDish(Integer.parseInt(id));
            for(Dish dish : existingDishListForTheMeal){
                if(dishToAdd.getName().equals(dish.getName())){
                    doesContain = true;
                    break;
                }
            }
        }
        return doesContain;
    }

    public void deleteDishFromMenu(HttpServletRequest request, HttpServletResponse response,
                                   Meal meal, List<Dish> existingDishListForTheMeal) throws IOException, ServletException {
        int idToDelete = Integer.valueOf(request.getParameter("button"));
        for (int i = 0; i < existingDishListForTheMeal.size(); i++) {
            Dish dish = existingDishListForTheMeal.get(i);
            if (dish.getId()==idToDelete) {
                existingDishListForTheMeal.remove(i);
                i--;
            }
        }

        mealService = new MealService();
        if(existingDishListForTheMeal.size()==0){
            mealService.delete(meal);
            response.sendRedirect("/mealmanager/home");
        }else{
            mealService.updateMeal(meal);

            request.setAttribute("meal", meal);

            request.getRequestDispatcher("/edit.jsp").forward(request, response);
        }
    }

    public WeekDay getDay(HttpServletRequest request) {
       return super.getDay(request);
    }

    public MealType getType(HttpServletRequest request) {
        return super.getType(request);
    }
}
