package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.UserDao;
import net.therap.mealmanager.domain.User;

import java.sql.SQLException;

/**
 * @author arafat
 * @since 11/29/16
 */
public class UserService {
    private UserDao userDao;

    public UserService(){
        userDao = new UserDao();
    }

    public boolean getUserByName(String username, String password){
        return userDao.getUser(username, password);
    }

    public void saveUser(User user) throws SQLException, ClassNotFoundException {
        userDao.save(user);
    }
}
