package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.WeekDayDao;
import net.therap.mealmanager.domain.WeekDay;

import java.util.List;

/**
 * @author arafat
 * @since 11/24/16
 */
public class WeekDayService {

    private WeekDayDao weekDayDao;

    public WeekDayService(){
        weekDayDao = new WeekDayDao();
    }

    public WeekDay getWeekDay(int dayId){
        return weekDayDao.findById(dayId);
    }

    public List<WeekDay> getListOfWeekDays(){
        return weekDayDao.findAll();
    }
}
