package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.MealTypeDao;
import net.therap.mealmanager.domain.MealType;

import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
public class MealTypeService {

    private MealTypeDao mealTypeDao;

    public MealTypeService(){
        mealTypeDao = new MealTypeDao();
    }

    public void saveMealType(MealType mealType) throws ClassNotFoundException, SQLException {
        mealTypeDao.save(mealType);
    }

    public List<MealType> getAllMealType() throws ClassNotFoundException, SQLException {
        return mealTypeDao.findAll();
    }

    public MealType getMealType(int id){
        return mealTypeDao.findById(id);
    }

    public void updateMealType(MealType mealType) throws ClassNotFoundException, SQLException {
        mealTypeDao.update(mealType);
    }
}
