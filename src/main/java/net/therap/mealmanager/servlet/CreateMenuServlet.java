package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.service.WeekDayService;
import net.therap.mealmanager.servlethelper.CreateMenuServletHelper;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/30/16
 */
@WebServlet("/createmenu")
public class CreateMenuServlet extends HttpServlet {
    private MealTypeService mealTypeService;
    private WeekDayService weekDayService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        mealTypeService = new MealTypeService();
        MealType type = mealTypeService.getMealType(Integer.valueOf(request.getParameter("type")));

        weekDayService = new WeekDayService();
        WeekDay day = weekDayService.getWeekDay(Integer.valueOf(request.getParameter("day")));

        Meal meal = null;
        MealService mealService = new MealService();
        try {
            meal = mealService.findMealByTypeAndDay(type, day);
        }catch (NullPointerException ex){

        }

        CreateMenuServletHelper helper = new CreateMenuServletHelper();

        if(meal != null){
            try {
                helper.reloadPageWithErrorMessage(request, response, weekDayService.getListOfWeekDays(),
                        mealTypeService.getAllMealType());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
                helper.saveMealAndRedirectToEditPage(day, type, request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        DishService dishService = new DishService();
        request.setAttribute("dishList", dishService.getAllDishes());

        mealTypeService = new MealTypeService();
        List<MealType> allMealTypes = null;
        try {
            allMealTypes = mealTypeService.getAllMealType();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(allMealTypes.size() == 0){
            response.sendRedirect("/mealmanager/mealtypes");

        }else{
            request.setAttribute("mealTypeList", allMealTypes);

            weekDayService = new WeekDayService();
            request.setAttribute("weekdays", weekDayService.getListOfWeekDays());

            request.getRequestDispatcher("/createmenu.jsp").forward(request, response);

        }

    }

}