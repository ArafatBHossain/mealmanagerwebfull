package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.servlethelper.DTOUtilityForEditServlet;
import net.therap.mealmanager.validation.Error;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author arafat
 * @since 11/30/16
 */
@WebServlet("/edit")
public class EditDishFromMealServlet extends HttpServlet {
    private MealService mealService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

       DTOUtilityForEditServlet dtoUtilityForEditServlet = new DTOUtilityForEditServlet();

       MealType type = dtoUtilityForEditServlet.getType(request);

       WeekDay day = dtoUtilityForEditServlet.getDay(request);

       mealService = new MealService();
       Meal meal = mealService.findMealByTypeAndDay(type,day);

       List<Dish> existingDishListForTheMeal = meal.getListOfDishes();

       if(request.getParameter("button").equals("add")){
           String[] dishesToAdd = null;
           try{
               dishesToAdd = request.getParameterValues("dishesToAdd");
           }catch (NullPointerException e){

           }

           if(dishesToAdd != null){
               dtoUtilityForEditServlet.addDishToMenu(request, response, meal, existingDishListForTheMeal);
           }else{

               Error error = new Error("No Dish Selected","Please Select Dish To Edit This Meal");
               request.setAttribute("meal", meal);
               request.setAttribute("errorMessage", error);
               request.getRequestDispatcher("/edit.jsp").forward(request, response);
           }

       }else{
               dtoUtilityForEditServlet.deleteDishFromMenu(request, response,  meal, existingDishListForTheMeal);
       }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        mealService = new MealService();
        Meal meal = mealService.getMealById((Integer) request.getSession().getAttribute("mealId"));
        request.setAttribute("meal", meal);

        DishService dishService = new DishService();
        request.getSession().setAttribute("allDishes", dishService.getAllDishes());

        request.getRequestDispatcher("/edit.jsp").forward(request, response);
    }
}
