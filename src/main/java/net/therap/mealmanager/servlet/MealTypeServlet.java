package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.MealTypeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/29/16
 */
@WebServlet("/mealtypes")
public class MealTypeServlet extends HttpServlet {

    private MealTypeService mealTypeService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        populateMealType(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            createMealAndReloadPage(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void populateMealType(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        mealTypeService = new MealTypeService();
        try {
            List<MealType> mealTypeList = mealTypeService.getAllMealType();

            if(mealTypeList.size() == 0){
                req.setAttribute("errorMessage", "No Meal Type Found, Add A Meal Type");
            }

            req.setAttribute("mealTypeList", mealTypeList);
            req.getRequestDispatcher("/mealtypes.jsp").forward(req, resp);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void createMealAndReloadPage(HttpServletRequest req, HttpServletResponse resp)
            throws SQLException, ClassNotFoundException, ServletException, IOException {
        mealTypeService = new MealTypeService();
        String mealTypeName = req.getParameter("name");
        MealType mealType = new MealType(mealTypeName);

        mealTypeService.saveMealType(mealType);

        resp.sendRedirect("/mealmanager/mealtypes");

    }
}
