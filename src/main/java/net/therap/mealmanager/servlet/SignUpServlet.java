package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.User;
import net.therap.mealmanager.service.UserService;
import net.therap.mealmanager.servlethelper.DTOUtiltiyForSignupServlet;
import net.therap.mealmanager.validation.Error;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author arafat
 * @since 11/30/16
 */
@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        Error errorResult = checkInput(userName, password, confirmPassword);

        if(errorResult.getType().equals("FALSE")){
            User user = new User(userName, password);
            UserService userService = new UserService();

            try {
                userService.saveUser(user);

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            setSession(user, request, response);

        }else{
            request.setAttribute("signUpErrorMessage",errorResult.getType()+": "+errorResult.getMessage());
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }

    }

    private Error checkInput(String userName, String password, String confirmPassword) {
        Error error;

        if(DTOUtiltiyForSignupServlet.userExists(userName, password)){

            error = new Error("Username","Already Available");

        }else{
            if(userName.equals("")|| password.equals("") || confirmPassword.equals("")){
                error = new Error("Empty field","Input field cannot be empty");
            }else if(!password.equals(confirmPassword)){
                error = new Error("Mismatch", "Please check password and confirm password");
            }else{
                error = new Error("FALSE","FALSE");
            }
        }

        return error;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    public void setSession(User user, HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        HttpSession session = request.getSession();

        session.setAttribute("name",user.getName());
        session.setAttribute("password",user.getPassword());
        session.setAttribute("auth_token",true);
        System.out.println("You are logged in as a valid user");
        response.sendRedirect("/mealmanager/home");
    }
}
