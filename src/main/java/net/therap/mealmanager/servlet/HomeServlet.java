package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.service.WeekDayService;
import net.therap.mealmanager.servlethelper.DTOUtilityForHomeServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/29/16
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DTOUtilityForHomeServlet dtoUtilityForHomeServlet = new DTOUtilityForHomeServlet();

        MealType type = dtoUtilityForHomeServlet.getType(request);

        WeekDay day = dtoUtilityForHomeServlet.getDay(request);

        MealService mealService = new MealService();
        Meal meal = null;
        try {
             meal = mealService.findMealByTypeAndDay(type, day);
        }catch (NullPointerException e){

        }

        if(meal == null){
            if(request.getParameter("button").equals("showMenu")){
                request.setAttribute("errorMessage", "Menu Not Prepared For the Meal");
            }else if(request.getParameter("button").equals("editMenu")){
                request.setAttribute("errorMessage", "Cannot Edit,Menu Not Prepared For the Meal");
            }
            dtoUtilityForHomeServlet.showMenuForTheMeal(0, request, response, day, type);

        }else{
            if(request.getParameter("button").equals("showMenu")){
                dtoUtilityForHomeServlet.showMenuForTheMeal(meal.getId(), request, response, day, type);
            }else if(request.getParameter("button").equals("editMenu")){
                dtoUtilityForHomeServlet.goToEditPage(meal.getId(), request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        MealTypeService mealTypeService = new MealTypeService();
        List<MealType> listOfAllMealTypes = null;

        try {
            listOfAllMealTypes = mealTypeService.getAllMealType();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(listOfAllMealTypes.size() == 0){
            response.sendRedirect("/mealmanager/mealtypes");
        }else {
            request.setAttribute("mealTypeList", listOfAllMealTypes);

            WeekDayService weekDayService = new WeekDayService();
            request.setAttribute("weekdays", weekDayService.getListOfWeekDays());

            request.getRequestDispatcher("/home.jsp").forward(request, response);
        }
    }
}
