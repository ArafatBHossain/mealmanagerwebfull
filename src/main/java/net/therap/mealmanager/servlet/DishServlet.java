package net.therap.mealmanager.servlet;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.service.DishService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author arafat
 * @since 11/29/16
 */
@WebServlet("/dishes")
public class DishServlet extends HttpServlet {

    private DishService dishService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        populateDishes(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            createDishAndReloadPage(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void populateDishes(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        dishService = new DishService();
        List<Dish> dishList = dishService.getAllDishes();

        req.setAttribute("dishList", dishList);
        req.getRequestDispatcher("/dishes.jsp").forward(req, resp);

    }

    private void createDishAndReloadPage(HttpServletRequest req, HttpServletResponse resp)
            throws SQLException, ClassNotFoundException, ServletException, IOException {
        dishService = new DishService();
        String dishName = req.getParameter("name");
        System.out.println(dishName);
        Dish dish = new Dish(dishName);
        dishService.saveDish(dish);

        resp.sendRedirect("/mealmanager/dishes");
    }

}
