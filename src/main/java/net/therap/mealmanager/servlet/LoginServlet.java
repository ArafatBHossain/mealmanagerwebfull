package net.therap.mealmanager.servlet;

import net.therap.mealmanager.service.UserService;
import net.therap.mealmanager.validation.Error;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author arafat
 * @since 11/28/16
 */
@WebServlet(urlPatterns = {"","/login"})
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkSessionForUser(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        validateAndSetSessionForUser(req, resp);
    }

    private void checkSessionForUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try{
            HttpSession session = req.getSession();
            if(session == null){
                resp.sendRedirect("login.jsp");
            }
            else if(session.getAttribute("auth_token")!=null &&
                    session.getAttribute("auth_token")=="true"){
                try {
                    resp.sendRedirect("/mealmanager/home");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                try {
                    req.getRequestDispatcher("login.jsp").forward(req, resp);
                } catch (ServletException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
             }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private boolean userExists(String username, String password) {
        UserService userService = new UserService();
        return userService.getUserByName(username, password);
    }

    private void validateAndSetSessionForUser(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        HttpSession session = req.getSession();

        String username = req.getParameter("name");
        String password = req.getParameter("password");
        if(userExists(username,password)){
            session.setAttribute("name",username);

            session.setAttribute("password",password);
            session.setAttribute("auth_token","true");
            resp.sendRedirect("/mealmanager/home");

        }else{
            Error error = new Error("Login", "Invalid Username or Password");

            session.setAttribute("auth_token","false");

            req.setAttribute("errorMessage", error.getType()+": "+error.getMessage());
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }
}
