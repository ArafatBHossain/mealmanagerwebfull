package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.User;
import net.therap.mealmanager.dbhelper.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author arafat
 * @since 11/29/16
 */
public class UserDao extends GenericDaoImpl<User, Integer> {

    public UserDao(){
        super(User.class);
    }

    public boolean getUser(String name, String password){
        Session session = HibernateUtil.getSessionAnnotationFactory().getCurrentSession();
        session.beginTransaction();
        User user ;
        boolean exists = false;
        try {
            Query query = session.createQuery("SELECT o FROM " + User.class.getName() + " o WHERE" +
                    " o.name = :name AND o.password = :password");
            System.out.println(query);
            query.setParameter("name", name);
            query.setParameter("password", password);

            user = (User) query.list().get(0);

            if(user!=null && user.getName().equals(name)){
                exists = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            session.getTransaction().commit();
        }
        return exists;
    }
}
