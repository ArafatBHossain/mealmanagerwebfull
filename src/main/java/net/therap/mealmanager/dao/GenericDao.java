package net.therap.mealmanager.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/20/16
 */
public interface GenericDao<T, ID extends Serializable> {

    T findById(ID id) throws RuntimeException;

    List<T> findAll() throws RuntimeException;

    void save(T object) throws RuntimeException;

    void update(T object) throws RuntimeException;

    void delete (T object) throws RuntimeException;


 }
