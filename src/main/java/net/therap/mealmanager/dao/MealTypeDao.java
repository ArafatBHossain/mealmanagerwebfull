package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.MealType;

/**
 * @author arafat
 * @since 11/20/16
 */
public class MealTypeDao extends GenericDaoImpl<MealType, Integer> {

        public MealTypeDao(){
            super(MealType.class);
        }

}
