package net.therap.mealmanager.dao;

import net.therap.mealmanager.dbhelper.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/20/16
 */
public class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

   private SessionFactory sessionFactory;

   private Class<T> type;

   public GenericDaoImpl(Class<T> type){
       this.type = type;
       sessionFactory = HibernateUtil.getSessionAnnotationFactory();
   }

    @Override
    public T findById(ID id) throws RuntimeException {
        T ret = null;

        try{
            ret = findIdNativeType(id);

        }catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }


    @Override
    public List<T> findAll() throws RuntimeException {
        List<T> list = null;

        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        try {
            list = session.createQuery("SELECT o FROM " + type.getName() + " o").list();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
            return list;
    }

    @Override
    public void save(T object) throws RuntimeException {
        try{
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();

            session.saveOrUpdate(object);

            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(T object) throws RuntimeException {

        Session session = sessionFactory.getCurrentSession();

        try{

            session.beginTransaction();

            session.merge(object);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.getTransaction().commit();

        }
    }

    @Override
    public void delete(T object) throws RuntimeException {
        Session session = sessionFactory.getCurrentSession();

        try{

            session.beginTransaction();

            session.delete(object);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.getTransaction().commit();

        }
    }

    private T findIdNativeType(ID id) {
        T ret = null;

        if(id!=null){
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();
            try{

                ret = (T)session.get(type, id);

            }catch(Exception e){
                e.printStackTrace();
            }finally {
                session.getTransaction().commit();
            }
        }

        return ret;
    }

}
