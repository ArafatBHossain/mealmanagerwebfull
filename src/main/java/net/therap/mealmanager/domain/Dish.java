package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/13/16
 */
@Entity
@Table(name = "dish")
public class Dish implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "dish_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;


    public Dish(){}

    public Dish(String dishName) {
        this.setName(dishName);
    }

    public int getId() {
        return id;
    }

    public void setId(int dishId) {
        this.id = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String dishName) {
        this.name = dishName;
    }

}

