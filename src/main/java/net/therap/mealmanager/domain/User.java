package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author arafat
 * @since 11/28/16
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUId = 1L;
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    public User(){

    }

    public User(String name, String password){
        this.setName(name);
        this.setPassword(password);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
