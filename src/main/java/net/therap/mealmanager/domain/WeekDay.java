package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author arafat
 * @since 11/23/16
 */
@Entity
@Table(name = "weekday")
public class WeekDay implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "day_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "day")
    private String name;

    public WeekDay(String day) {
        this.setName(day);
    }

    public WeekDay() {
    }

    public String getName() {
        return name;
    }

    public void setName(String day) {
        this.name = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
