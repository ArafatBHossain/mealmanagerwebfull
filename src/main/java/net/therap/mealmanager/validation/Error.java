package net.therap.mealmanager.validation;

/**
 * @author arafat
 * @since 12/1/16
 */
public class Error {

    private String type;
    private String message;

    public Error(String type, String message){
        this.setType(type);
        this.setMessage(message);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
