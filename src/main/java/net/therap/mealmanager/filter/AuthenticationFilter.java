package net.therap.mealmanager.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author arafat
 * @since 11/28/16
 */
@WebFilter(urlPatterns = {"/home","/dishes","/mealtypes","/edit","/createmenu"})
public class AuthenticationFilter implements Filter {
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();

        if (session.getAttribute("auth_token")=="true") {
            chain.doFilter(req, resp);
        }else{
            response.sendRedirect("/mealmanager/login");
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
